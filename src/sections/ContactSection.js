import React from 'react';
import styled from 'styled-components';
import GlassesImage from '../images/svg/glasses.inline.svg';
import {
	Container,
	Col,
	Row
} from 'reactstrap';
import Button from '../components/Button';
import { device } from '../utils/devices';


const Section = styled.section`
	padding: 90px 0;
  background: #EDEDED;
`

const StyledGlassesImage = styled(GlassesImage)`
	@media ${device.lg} {
		margin-bottom: 0;
	}
	margin-bottom: 30px;
	width: 100%;
	max-width: 100%;
`

const ContactSection = () => {

	return(
		<Section>
			<Container>
				<Row className="mb-normal justify-content-between">
					<Col>
						<h3>Don’t see what you are looking for?</h3>
					</Col>
				</Row>
        <Row>
          <Col md={3}>
            <StyledGlassesImage />
          </Col>
          <Col lg={6}>
            <p>We offer a robust suite of screening services we can customize to your needs, from criminal background checks to workforce monitoring and robust ATS integrations.</p>
          </Col>
          <Col lg={3}>
            <Button block="true" href="https://www.sterlingcheck.com/contact/" target="_blank">Contact Us</Button>
          </Col>
        </Row>
			</Container>
		</Section>
	)
}

export default ContactSection;
