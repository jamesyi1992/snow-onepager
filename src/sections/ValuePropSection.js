import React from 'react';
import styled from 'styled-components';
import {
	Container,
	Row
} from 'reactstrap';
import ShieldIcon from '../images/svg/shield.inline.svg';
import WatchIcon from '../images/svg/watch.inline.svg';
import CheckmarkIcon from '../images/svg/checkmark.inline.svg';
import Fade from 'react-reveal/Fade';

import IconCallout from '../components/IconCallout';
import StyledCol from '../components/StyledCol';

const StyledSection = styled.section`
	padding: 60px 0;
`

const ValuePropSection = () => {
	return (
		<StyledSection>
			<Container>
				<Row>
					<StyledCol lg={4}>
						<Fade>
							<IconCallout 
								Icon={ShieldIcon} 
								title="Trustworthy"
								content="Rigorous, comprehensive background checks leverage Sterling's decades of industry-leading experience."
							/>
						</Fade>
					</StyledCol>
					<StyledCol lg={4}>
						<Fade delay={300}>
							<IconCallout 
								Icon={WatchIcon} 
								title="Convenient"
								content="Start in minutes. 90% of criminal background checks are complete within a day."
							/>
						</Fade>
					</StyledCol>
					<StyledCol lg={4}>
						<Fade delay={600}>
							<IconCallout 
								Icon={CheckmarkIcon} 
								title="Compliant"
								content="SterlingNOW is designed to help you comply with the Fair Credit Reporting Act (FCRA)."
							/>
						</Fade>
					</StyledCol>
				</Row>
			</Container>
		</StyledSection>
	)
}

export default ValuePropSection;