import React from 'react';
import styled from 'styled-components';
import {
	Container,
	Col,
	Row
} from 'reactstrap';

import PricingBox from '../components/PricingBox';
import CenterHeading from '../components/CenterHeading';
import PriceTag from '../components/PriceTag';
import Fade from 'react-reveal/Fade';

import StyledCol from '../components/StyledCol';

import { device } from '../utils/devices';


const StyledSection = styled.section`
	padding: 90px 0;
	background: #F6F6F6;
`

const PricingTagWrap = styled(Col)`
	display: flex;
	flex-direction: column;
	@media ${device.lg} {
		flex-direction: row;
	}
	justify-content: space-between;
`

const PricingCaption = styled(Col)`
	text-align: center;
`

const priceTagData = [
	{
		title: "Driver's Check",
		price: '$9.95'
	},
	{
		title: "Education Verification",
		price: '$19.95'
	},
	{
		title: "Employment Verification",
		price: '$19.95'
	}
]

const PricingSection = () => {
	return(
		<StyledSection>
			<Container>
				<CenterHeading>
					Choose a background check that works for you.
				</CenterHeading>
				<Row className="mb-big">
					<StyledCol lg={4}>
						<Fade>
							<PricingBox 
								title="Basic Check"
								price="$29.95"
								content="For businesses with smaller budgets, this check covers the basics. Includes a multi-state criminal database search, a sex offender registry search, and a single-county primary search."
								list={
									{
										title: 'Includes:',
										list: [
											'Social Security Number Trace',
											'Single County Criminal Record Search',
											'Multi-State Instant Criminal Check with Verification',
											'DOJ National Sex Offender Public Website Search'
										]
									}
								}
								button={{
									text: 'Start With Basic'
								}}
							/>
						</Fade>
					</StyledCol>
					<StyledCol lg={4}>
						<Fade delay={300}>
							<PricingBox 
								active={true}
								title="Preferred Check"
								price="$59.95"
								content="Includes all the features of Basic, and for added thoroughness extends the search to court-level records in every county where your candidate has lived in the past seven years."
								list={{
									title: 'Includes all the features of the Basic Check plus:',
									list: [
										'Federal Criminal District Search',
										'Office of Foreign Asset Control (OFAC) Terrorist Watchlist'
									]
									}
								}
								button={{
									text: 'Start With Preferred'
								}}
							/>
						</Fade>
					</StyledCol>
					<StyledCol lg={4}>
						<Fade delay={600}>
							<PricingBox 
								title="Pro Check"
								price="$79.95"
								content="Our most comprehensive background check includes all the features of Basic and Preferred, plus Locator Select, exclusive to Sterling, which searches booking records from 3,500+ jurisdictions to add an additional layer of rigor."
								list={
									{
										title: 'Includes all the features of the Preferred Check plus:',
										list: [
											'7-Year Federal District Criminal Record Search',
											'Locator Select with Verification'
										]
									}
								}
								button={{
									text: 'Start With Pro'
								}}
							/>
						</Fade>
					</StyledCol>
				</Row>
				<CenterHeading h3 spacing="mb-normal">
					Easily customize any background check with add-ons.
				</CenterHeading>
				<Row className="justify-content-center mb-normal">
					<PricingTagWrap md="8">
						{
							priceTagData.map((tag, index) => {
								return(
									<PriceTag
										title={tag.title}
										price={tag.price}
										key={index}
									/>
								)
							})
						}
					</PricingTagWrap>
				</Row>
				<Row>
					<PricingCaption>Taxes and local fees not included.</PricingCaption>
				</Row>
			</Container>
		</StyledSection>
	)
}

export default PricingSection;
