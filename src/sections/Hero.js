import React from 'react';
import { graphql, useStaticQuery } from 'gatsby';
import styled from 'styled-components';

import Button from '../components/Button';

import {
	Col,
	Container,
	Row
} from 'reactstrap';

import BackgroundImage from 'gatsby-background-image';
import {useSpring, animated} from 'react-spring';

import { device } from '../utils/devices';




const StyledBackground = styled(BackgroundImage)`
	display: flex;
	align-items: center;
	height: 587px;
`

const StyledLineHeader = styled.h1`
	position: relative;
	margin-top: 30px;
	&::before {
		content: '';
		position: absolute;
		background: #FE4D09;
		width: 60px;
		height: 10px;
		top: -30px;
		left: 0;
	}
`

const Wrap = styled.div`
	h1 {
		@media ${device.lg} {
			width: 670px;
		}
		letter-spacing: -0.32px;
		margin-bottom: 15px;
		color: #ffffff;
	}
	p {
		@media ${device.lg} {
			width: 526px;
		}
		margin-bottom: 90px;
		color: #ffffff;
	}
`

const Hero = () => {

	const data = useStaticQuery(graphql`
		query HeaderImageQuery {
		  bgImg:file(relativePath: {eq: "snow-header.jpg"}) {
		    childImageSharp {
		      fluid(quality: 90, maxWidth: 4160) {
		        ...GatsbyImageSharpFluid
		      }
		    }
		  }
		}
	`)

	const props = useSpring(
		{
			opacity: 1,
			transform: 'translate3d(0, 0, 0)',
			from: {
				opacity: 0,
				transform: 'translate3d(0, 30px, 0)',
			}
		}
	);

	const props2 = useSpring(
		{
			opacity: 1,
			delay: 500,
			from: {
				opacity: 0
			}
		}
	);

	const AnimatedHeader = animated(StyledLineHeader);

	const AnimatedContent = animated.p;

	return (
		<section>
			<StyledBackground
				fluid={ data.bgImg.childImageSharp.fluid }
			>
				<Container>
					<Row>
						<Col>
							<Wrap>
								<AnimatedHeader style={props}>Order a trusted background check in minutes.</AnimatedHeader>
								<AnimatedContent style={props2} className="lead-1">SterlingNOW offers small businesses and franchises thorough, high-quality background checks from a global leader in screening.</AnimatedContent>
								<Button href="https://go.sterlingnow.io/account-setup/register">Start a Background Check</Button>
							</Wrap>
						</Col>
					</Row>
				</Container>
			</StyledBackground>
		</section>
	)

}

export default Hero;