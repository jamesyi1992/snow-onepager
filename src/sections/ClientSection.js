import React from 'react';
import styled from 'styled-components';
import { useStaticQuery, graphql } from 'gatsby';
import {
	Container,
	Col,
	Row
} from 'reactstrap';
import Img from 'gatsby-image';
import StyledCol from '../components/StyledCol';
import { device } from '../utils/devices';


const Section = styled.section`
	padding: 90px 0;
`

const StyledCenterHeading = styled.h3`
  @media ${device.lg} {
    width: 800px;
      &::before {
        content: '';
        position: absolute;
        width: 100px;
        height: 100px;
        background: #ADD7C7;
        opacity: .6;
        left: -60px;
        top: -20px;
        z-index: -1;
      }

      &::after {
        content: '';
        position: absolute;
        width: 48px;
        height: 48px;
        background: #FF4700;
        opacity: .5;
        left: -90px;
        bottom: -40px;
        z-index: -1;
      }
  }
  position: relative;
	text-align: left;
	margin: 0 auto;
`

const ImgWrap = styled.div`
	padding: 30px 60px;
`

const ClientSection = () => {

	const data = useStaticQuery(graphql`
    query ClientImagesQuery {
      client1: file(relativePath: { eq: "sylvan-logo.jpg" }) {
        childImageSharp {
          fluid(maxWidth: 500) {
            ...GatsbyImageSharpFluid
          }
        }
      }
      client2: file(relativePath: { eq: "salesloft-logo.jpg" }) {
        childImageSharp {
          fluid(maxWidth: 500) {
            ...GatsbyImageSharpFluid
          }
        }
      }
      client3: file(relativePath: { eq: "seattle-cancer-care-logo.jpg" }) {
        childImageSharp {
          fluid(maxWidth: 500) {
            ...GatsbyImageSharpFluid
          }
        }
      }
      client4: file(relativePath: { eq: "blaze-pizza-logo.jpg" }) {
        childImageSharp {
          fluid(maxWidth: 500) {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  `)

	return(
		<Section>
			<Container>
				<Row className="mb-big">
					<Col>
            <StyledCenterHeading>
              SterlingNOW is from Sterling, which has proudly served clients from startups to Fortune 500 companies for over 40 years.
            </StyledCenterHeading>
          </Col>
				</Row>
				<Row>
					{
						Object.values(data).map((image, index) => {
							return (
								<StyledCol key={index} lg={3} md={4} xs={6}>
									<ImgWrap>
										<Img fluid={image.childImageSharp.fluid} />
									</ImgWrap>
								</StyledCol>
							)
						})
					}
				</Row>
			</Container>
		</Section>
	)
}

export default ClientSection;
