import React, { Component } from 'react';
import { StaticQuery, graphql } from 'gatsby';
import { Parallax, ParallaxLayer } from 'react-spring/renderprops-addons'
import {
	Row,
	Col,
	Container
} from 'reactstrap';
import styled from 'styled-components';
import Img from 'gatsby-image';

const StyledParallaxLayer = styled(ParallaxLayer)`
	display: flex;
	align-items: center;
`

const Content = styled.div`
	position: relative;
	z-index: 2;
	width: 360px;

	p {
		font-size: 20px;
		line-height: 26/20;
		margin-bottom: 0;
	}
`

class ParallaxSection extends Component {
	render() {
		return (
			<StaticQuery
		    query={graphql`
		      query ImagesQuery2 {
			      loginImage: file(relativePath: { eq: "Login@2x.jpg" }) {
			        childImageSharp {
			          fluid(maxWidth: 2000) {
			            ...GatsbyImageSharpFluid
			          }
			        }
			      }
			    }
		    `}
		    render={data => (
		      <Parallax ref={ref => (this.parallax = ref)} pages={3}>
						<StyledParallaxLayer offset={0} factor={0.5} speed={1} style={{ backgroundColor: '#f6f6f6' }}>
							<Container>
								<Row className="align-items-center">
									<Col md="5">
										<Content>
											<h3>Create an account in no time</h3>
											<p>Our self-service website is ready when you are. No sales calls or setup fees. Order your first background check in minutes.</p>
										</Content>
									</Col>
									<Col md="7">
									</Col>
								</Row>
							</Container>
						</StyledParallaxLayer>

						<StyledParallaxLayer offset={0.5} factor={0.5} speed={2} style={{ backgroundColor: '#f6f6f6' }}>
							<Container>
								<Row className="align-items-center">
									<Col md="5">
										<Content>
											<h3>Create an account in no time</h3>
											<p>Our self-service website is ready when you are. No sales calls or setup fees. Order your first background check in minutes.</p>
										</Content>
									</Col>
									<Col md="7">
									</Col>
								</Row>
							</Container>
						</StyledParallaxLayer>

						</Parallax>
		    )}
		  />
		);
	}
}

export default ParallaxSection;
