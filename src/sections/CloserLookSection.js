import React from 'react';
import { useStaticQuery, graphql } from "gatsby";
import styled from 'styled-components';
import {
	Container,
	Row
} from 'reactstrap';
import CenterHeading from '../components/CenterHeading';
import StyledCol from '../components/StyledCol';
import SvgWave from '../images/svg/wave-bg.inline.svg';
import Img from 'gatsby-image';
import ParticleCluster from '../components/ParticleCluster';
import { device } from '../utils/devices';
import Fade from 'react-reveal/Fade';
import { useSpring, animated } from 'react-spring';


const Section = styled.section`
	padding: 0 0 90px;
	background: #f6f6f6;
	position: relative;
	z-index: 1;
`

const StyledImg = styled(Img)`
	position: relative;
	z-index: 2;
	box-shadow: 0 5px 20px 0 rgba(0,0,0,.1);
`

const Content = styled.div`
	position: relative;
	z-index: 2;

	@media ${device.lg} {
		width: 400px;
	}

	p {
		font-size: 1.25rem;
		line-height: 26/20;
		margin-bottom: 0;
	}
`

const StyledSvgWave = styled(SvgWave)`
	position: absolute;
	bottom: 100px;
	z-index: -1;
	left: 0;
	width: 100%;
`


const CloserLookSection = () => {

	const props = useSpring({
		opacity: 1,
		delay: 2000,
		from: {
			opacity: 0
		}
	});

	const Animated = animated.h3;

	const data = useStaticQuery(graphql`
    query ImagesQuery {
      loginImage: file(relativePath: { eq: "Login@2x.jpg" }) {
        childImageSharp {
          fluid(maxWidth: 1000) {
            ...GatsbyImageSharpFluid
          }
        }
      }
      productImage: file(relativePath: { eq: "product-select.jpg" }) {
        childImageSharp {
          fluid(maxWidth: 1000) {
            ...GatsbyImageSharpFluid
          }
        }
      }
      rulesImage: file(relativePath: { eq: "rules.jpg" }) {
        childImageSharp {
          fluid(maxWidth: 1000) {
            ...GatsbyImageSharpFluid
          }
        }
      }
      payImage: file(relativePath: { eq: "pay-as-you-go.jpg" }) {
        childImageSharp {
          fluid(maxWidth: 1000) {
            ...GatsbyImageSharpFluid
          }
        }
      }
      resultImage: file(relativePath: { eq: "results.jpg" }) {
        childImageSharp {
          fluid(maxWidth: 1000) {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  `)

	return(
		<React.Fragment>
			<Section>
				<Container>
					<CenterHeading>
						Take a closer look at SterlingNOW.
					</CenterHeading>
					<Row className="align-items-center mb-extra">
						<StyledCol lg={{ size: 7, order: 1 }}>
							<Fade right>
								<StyledImg fluid={data.loginImage.childImageSharp.fluid} alt="Create an account in no time" />
							</Fade>
						</StyledCol>
						<StyledCol lg={{ size: 5, order: 0 }}>
							<Content>
								<h3>Create an account in no time</h3>
								<p>Our self-service website is ready when you are. No sales calls or setup fees. Order your first background check in minutes.</p>
							</Content>
						</StyledCol>
					</Row>
					<Row className="align-items-center mb-extra">
						<StyledCol lg={{ size: 7, order: 0 }}>
							<Fade left>
								<StyledImg fluid={data.productImage.childImageSharp.fluid} alt="Run a check you can trust" />
							</Fade>
						</StyledCol>
						<StyledCol lg={{ size: 5, order: 1 }} className="d-flex justify-content-end">
							<Content className="position-relative">
								<h3>Run a check you can trust</h3>
								<p>Sterling uses exclusive technology and thorough, multi-step processes that deliver trusted results, catching issues that can that can fall through the cracks otherwise.</p>
									<ParticleCluster x={[-30, 30]} y={[-20, 20]} />
							</Content>
						</StyledCol>
					</Row>
					<Row className="align-items-center mb-extra">
						<StyledCol lg={{ size: 7, order: 0 }}>
							<Fade left>
								<StyledImg fluid={data.rulesImage.childImageSharp.fluid} alt="Stick to the rules" />
							</Fade>
						</StyledCol>
						<StyledCol lg={{ size: 5, order: 1 }} className="d-flex justify-content-end">
							<Content>
								<h3>Stick to the rules</h3>
								<p>Regulations are constantly changing at the federal, state, and local levels. SterlingNOW is designed to help you comply with the FCRA. Adverse action tools are included.</p>
							</Content>
						</StyledCol>
					</Row>
					<Row className="align-items-center mb-extra">
						<StyledCol lg={{ size: 7, order: 1 }}>
							<Fade right>
								<StyledImg fluid={data.payImage.childImageSharp.fluid} alt="Pay as you go" />
							</Fade>
						</StyledCol>
						<StyledCol lg={{ size: 5, order: 0 }}>
							<Content>
								<h3>Pay as you go</h3>
								<p>SterlingNOW is flexible to your needs. Pay just for the background checks you order, with no additional setup fees or costly subscriptions.</p>
								<ParticleCluster x={[-30, 30]} y={[10, -10]}/>
							</Content>
						</StyledCol>
					</Row>
					<Row className="align-items-center">
						<StyledCol lg={{ size: 7, order: 1 }}>
							<Fade left>
								<StyledImg style={props} fluid={data.resultImage.childImageSharp.fluid} alt="Get results quickly" />
							</Fade>
						</StyledCol>
						<StyledCol lg={{ size: 5, order: 1 }} className="d-flex justify-content-end">
							<Content>
								<Animated style={props}>Get results quickly</Animated>
								<p>90% of criminal background checks are complete within 24 hours. Conveniently manage reports in an intuitive dashboard.</p>
							</Content>
						</StyledCol>
					</Row>
				</Container>
				<StyledSvgWave />
			</Section>
		</React.Fragment>
	)
}

export default CloserLookSection;