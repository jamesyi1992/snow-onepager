import React from 'react';
import styled from 'styled-components';
import { Parallax } from 'react-scroll-parallax';

import { device } from '../utils/devices';

const Wrap = styled.div`
	position: absolute;
	bottom: -220px;
	left: 60px;
`

const StyledParticleCluster = styled.div`
	display: none;

	@media ${device.lg} {
		display: block;
	}
	
	position: relative;
	width: 146px;
	height: 146px;
	background: #D9E8E8;
	opacity: .8;
	z-index: -1;
	&::before {
		content: '';
		position: absolute;
		width: 70px;
		height: 70px;
		background: #FA6400;
		opacity: .6;
		top: -30px;
		left: -25px;
		z-index: -1;
	}
`

const ParticleCluster = ({x, y}) => {
	return(
		<Wrap>
			<Parallax y={y} x={x}>
				<StyledParticleCluster />
			</Parallax>
		</Wrap>
	)
}

export default ParticleCluster;
