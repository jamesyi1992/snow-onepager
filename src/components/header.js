import { Link } from "gatsby"
import PropTypes from "prop-types"
import React from "react"
import styled from 'styled-components';

import { Container } from 'reactstrap';

import SterlingNowLogo from '../images/svg/snow-logo.inline.svg';

const StyledHeader = styled.header`

  display: flex;
  height: 96px;
  align-items: center;

`

const StyledLogo = styled(SterlingNowLogo)`
  
  width: 108px;
  height: 40px;

`

const Header = ({ siteTitle }) => (
  <StyledHeader>
    <Container>
      <Link to="/">
        <StyledLogo />
      </Link>
    </Container>
  </StyledHeader>
)

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
