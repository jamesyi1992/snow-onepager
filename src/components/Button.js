import styled from 'styled-components';
import { Link } from 'gatsby';



const StyledButton = styled.a`
	display: ${props => props.block ? 'block' : 'inline-block'};
	text-align: center;
	cursor: pointer;
	background: ${props => props.ghost === 'true' ? '#ffffff' : '#FE4D09'};
	padding: ${props => props.small ? '10px 40px' : '15px 60px'};
	color: ${props => props.ghost === 'true' ? '#FE4D09' : '#ffffff'};
	border: 2px solid #FE4D09;
	text-transform: uppercase;
	border-radius: 6px;
	text-decoration: none;
	font-weight: bold;
	transition: .2s all ease-in-out;
	font-size: ${props => props.small === 'true' ? '12px' : 'inherit'};

	&:hover {
		background: ${props => props.ghost === 'true' ? '#FE4D09' : '#cc3900'};
		border: 2px solid ${props => props.ghost === 'true' ? '#FE4D09' : '#cc3900'};;
		color: #ffffff;
	}

`

export default StyledButton;
