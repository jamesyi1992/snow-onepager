import React from 'react';
import styled from 'styled-components';
import { device } from '../utils/devices';

const StyledPriceTag = styled.div`
	display: flex;
	flex-direction: column;
	@media ${device.lg} {
		width: 200px;
	}
	box-shadow: 0 2px 4px #A4A4A4;
	height: 78px;
	align-items: center;
	justify-content: center;
	background: #ffffff;

	span {
		&:first-child {
			font-size: 15px;
			font-weight: normal;
		}
		font-weight: bold;
		font-size: 18px;
	}
`

const PriceTag = ({ title, price }) => {
	return(
		<StyledPriceTag>
			<span>{ title }</span>
			<span>{ price }</span>
		</StyledPriceTag>
	)
}

export default PriceTag;