import React from 'react';
import styled from 'styled-components';
import {
	Container,
	Col,
	Row
} from 'reactstrap';
import { device } from '../utils/devices';


const Section = styled.footer`
	padding: 30px 0;
  background: #F4F4F4;
`

const Links = styled.div`
	@media ${device.md} {
		display: flex;
		flex-direction: row;
		justify-content: end;
		order: 2;
	}

	display: flex;
	flex-direction: column;

	a {
		text-decoration: none;
		color: #737774;
		cursor: pointer;
		
		&:not(:last-child) {
			position: relative;
			margin-right: 20px;
			
			@media ${device.md} {
				&::after {
					content: '';
					width: 1px;
					height: 20px;
					background-color: #737774;
					position: absolute;
					top: 50%;
					right: -10px;
					transform: translateY(-50%);
				}
			}

		}

	}

`
const Copy = styled.div`
	@media ${device.md} {
		order: 1;
	}
`

const FlexCol = styled(Col)`
	@media ${device.md} {
		display: flex;
		justify-content: space-between;
	}
`

const Footer = () => {

	return(
		<Section>
			<Container>
				<Row className="justify-content-between">
					<FlexCol>
						<Links>
							<a target="_blank" href="https://www.sterlingcheck.com/contact/" rel="noopener noreferrer">Contact Us</a>
							<a target="_blank" href="https://www.sterlingcheck.com/about-sterling/fact-act-disclosure/" rel="noopener noreferrer">FACT Disclosure</a>
							<a target="_blank" href="https://www.sterlingcheck.com/about-sterling/privacy/" rel="noopener noreferrer">Privacy Policy</a>
						</Links>
						<Copy>
							&copy; { new Date().getFullYear()  } Sterling
						</Copy>
					</FlexCol>
        </Row>
			</Container>
		</Section>
	)
}

export default Footer;
