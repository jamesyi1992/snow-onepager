import React from 'react';
import styled from 'styled-components';
import Checkmark from '../images/checkmark.svg';
import CheckmarkActive from '../images/checkmark-active.svg';

import Button from '../components/Button';

const StyledPricingBox = styled.div`

`

const Head = styled.div`
	height: 73px;
	display: flex;
	box-shadow: 0 6px 8px #A4A4A4;
	padding: 0 20px;
	align-items: center;
	justify-content: space-between;
	background: ${props => props.active ? '#FE4D09' : '#4F535C'};

	h3 {
		margin-bottom: 0;
		color: #ffffff;
		font-size: 18px;
		line-height: 22/18;
	}

	span {
		&:first-child {
			font-size: 22px;
			font-weight: bold;
			font-family: 'Hind', sans-serif;
			margin-bottom: 0;
		}
		display: block;
		color: #ffffff;
		font-size: 11px;
		line-height: 1.1;
	}
`

const Wrap = styled.div`
	display: flex;
	flex-direction: column;
	justify-content: space-between;
	padding: 20px;
	min-height: 350px;
	background: #ffffff;
`

const Content = styled.div`
	p {
		font-size: 13px;
		line-height: 17/13;
	}
`

const List = styled.div`
	h4 {
		font-size: 14px;
		line-height: 22/14;
		margin-bottom: 10px;
	}

	ul {
		list-style: none;
		padding: 0;
		margin: 0;
	}

	li {
		&::before {
			content: '';
			top: 3px;
			left: 0;
			background: url(${props => props.active ? CheckmarkActive : Checkmark});
			background-size: contain;
			position: absolute;
			height: 14px;
			width: 14px;
		}
		position: relative;
		font-size: 12px;
		line-height: 20/12;
		padding-left: 25px;
		&:not(:last-child) {
			margin-bottom: 5px;
		})
	}
`

const Footer = styled.div`
	text-align: center;
`

const PricingBox = ({ active, title, price, content, list, button }) => {
	return (
		<StyledPricingBox>
			<Head active={active}>
				<h3>{ title }</h3>
				<div className="text-right">
					<span>{ price }</span>
					<span>Per Check</span>
				</div>
			</Head>
			<Wrap>
				<Content>
					<p>{ content }</p>
					<List active={active}>
						<h4>{list.title}</h4>
						<ul>
							{
								list.list.map((item, index) => {
									return(
										<li key={index}>{item}</li>
									)
								})
							}
						</ul>
					</List>
				</Content>
				<Footer>
					<Button href="https://go.sterlingnow.io/account-setup/register" ghost={ active ? 'false' : 'true' } small="true">
						Get Started
					</Button>
				</Footer>
			</Wrap>
		</StyledPricingBox>
	)
}

export default PricingBox;