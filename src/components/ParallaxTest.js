import React, { Component } from 'react';
import {Parallax, ParallaxLayer} from 'react-spring/renderprops-addons'


class ParallaxTest extends Component {
	render() {
		return (
			<Parallax ref={ref => (this.parallax = ref)} pages={3}>
			  <ParallaxLayer offset={1.3} speed={-0.3} style={{ pointerEvents: 'none' }}>
          <span>Ahahahah look at me</span>
        </ParallaxLayer>
			</Parallax>
		);
	}
}

export default ParallaxTest;