import React from 'react';
import styled from 'styled-components';
import { device } from '../utils/devices';

const StyledIconCallout = styled.div`
	
	text-align: center; 

	&:not(:last-child) {
		margin-bottom: 30px;
	}

	@media ${device.xl} {
		width: 340px;
		text-align: left;
	}

	svg {
		margin-bottom: 10px;
	}

	h3 {
		margin-bottom: 10px;
	}

	p {
		margin-bottom: 0;
	}
`

const IconCallout = ({ Icon, title, content }) => {
	return(
		<StyledIconCallout>
			<div className="text-center">
				{ Icon ? <Icon /> : null }
				<h3>{title}</h3>
			</div>
			<p>{content}</p>
		</StyledIconCallout>
	)
}

export default IconCallout;
