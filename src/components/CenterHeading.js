import React from 'react';
import {
	Col, 
	Row,
} from 'reactstrap';

const CenterHeading = ({ children, h3, spacing }) => {
	return(
		<Row className={ spacing ? spacing : 'mb-big' }>
			<Col className="text-center">
				{ 
					h3 ? <h3 className="mb-0">{ children }</h3> : <h2 className="mb-0">{ children }</h2>
				}
			</Col>
		</Row>
	)
}

export default CenterHeading;