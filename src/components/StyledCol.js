import styled from 'styled-components';
import { Col } from 'reactstrap';
import { device } from '../utils/devices';

const StyledCol = styled(Col)`
	&:not(:last-child) {
		margin-bottom: 30px;
	}

	@media ${device.xl} {
		&:not(:last-child) {
			margin-bottom: 0;
		}
	}
`

export default StyledCol;