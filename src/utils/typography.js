import Typography from 'typography';

const typography = new Typography({
  baseFontSize: '16px',
  baseLineHeight: 1.6,
  googleFonts: [
    {
      name: 'Hind',
      styles: ['700'],
    },
    {
      name: 'Open Sans',
      styles: ['400', '400i', '600', '600i'],
    },
  ],
  headerWeight: 700,
  bodyWeight: 400,
  boldWeight: 600,
  headerFontFamily: ['Hind', 'sans-serif'],
  bodyFontFamily: ['Open Sans', 'sans-serif'],
  overrideStyles: ({ rhythm }, options, styles) => ({
    h1: {
      fontSize: '2.75rem',
      lineHeight: '1.32',
      marginBottom: '30px',
    },
    h2: {
      fontSize: '2.125rem',
      lineHeight: 41/34,
      marginBottom: '30px',
    },
    h3: {
      fontSize: '1.625rem',
      lineHeight: 34/26,
      marginBottom: '30px',
    },
    p: {
      color: '#737774'
    },
    'p.lead-1': {
      fontSize: '1.5rem',
      lineHeight: 37/24,
    }
  }),
})

export default typography