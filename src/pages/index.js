import React from "react"
import { ParallaxProvider } from 'react-scroll-parallax';
import Layout from "../components/layout"
import SEO from "../components/seo"
import Hero from '../sections/Hero';
import ValuePropSection from '../sections/ValuePropSection';
import PricingSection from '../sections/PricingSection';
import CloserLookSection from '../sections/CloserLookSection';
import ClientSection from '../sections/ClientSection';
import ContactSection from '../sections/ContactSection';

const IndexPage = () => {

	return(
		<Layout>
			<ParallaxProvider>
		    <SEO 
		    	title="SterlingNOW - Order a Background Check in Under 5 Minutes" 
		    	description="SterlingNOW offers small businesses and franchises thorough, high-quality background checks. Order a background check in under five minutes." 
		    />
		    <Hero />
		    <ValuePropSection />
		    <PricingSection />
		    <CloserLookSection />
		    <ClientSection />
		    <ContactSection />
		  </ParallaxProvider>
	  </Layout>
	)
  
}


export default IndexPage
